/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import models.Editorial;
/**
 *
 * @author jaimearroyo
 */
public class EditorialController {
    private Editorial editorial;
/**
 * Constructor que setea el editorial como una nueva instancia de la clase Editorial
 */
    public EditorialController() {
        this.editorial = new Editorial();
    }
    
   /**
    * Metodo que setea el nombre del editorial en la instancia de la misma clase
    * @param editorial nombre del editorial
    */ 
    public void newone(String editorial){        
        this.editorial.setNombre(editorial);
    }
    
    /**
     * Metodo que recorre todos los editoriales y los va almacenando en un objecto anonimo para poder entregarselas a la vista
     * @return ArrayList con objectos de editoriales que contienen el id y el editorial
     */
    public static ArrayList<Object[]> index(){
        ArrayList<Object[]> editoriales  = new ArrayList<Object[]>();

        for(Editorial temp : Editorial.all()) {
            Object[] objs = new Object[]{temp.getId(),temp.getNombre()};
            editoriales.add(objs);
        }
        
        return editoriales;
    } 
    
    /**
     * Por medio del parametro id, usando el metodo publico y estatico find de la clase Editorial se busca ese editorial en la BD
     * @param id unico del editorial dentro de la base de datos
     * @return boolean true si obtuvo correctamente el registro y false si no encontro el registro en la base de datos
     */
    public boolean get(Integer id){
        this.editorial = Editorial.find(id);
        return this.editorial.getId() != -1;
    }
    
    /**
     * Metodo para gatillar el guardar de la clase Editorial, retornara el ID con el que se guardo en la BD
     * ya sea para actualizar o para la creación
     * @return Integer id de la entrada luego de guardar 
     */
    public Integer save(){
        Integer id;
        this.editorial.save();
        id = this.editorial.getId();
        return id;
    }
    
    /**
     * Metodo para consultar si el editorial dentro del controlador es una nueva o existente
     * @return boolean true si es nuevo registro o false si no lo es 
     */
    public boolean new_record(){
        return this.editorial.getId() == -1;
    }
    
    /**
     * Obtiene los campos del objeto de la instancia Editorial y los entrega en un objeto anonimo
     * @return Object anonimo con dos campos el id y el editorial
     */
    public Object[] get_fields(){        
        Object[] objs = new Object[2]; 
        objs[0] = this.editorial.getId();
        objs[1] = this.editorial.getNombre();
        return objs;
    }
    
    /**
     * Metodo para destruir el editorial actual
     */
    public void eliminar(){
       this.editorial.destroy();
       this.editorial = new Editorial();
    }
    
}
