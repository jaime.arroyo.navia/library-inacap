/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import models.Categoria;
/**
 *
 * @author jaimearroyo
 */
public class CategoriaController {
    private Categoria cate;
/**
 * Constructor que setea la categoria como una nueva instancia de la clase Categoria
 */
    public CategoriaController() {
        this.cate = new Categoria();
    }
    
   /**
    * Metodo que setea el nombre de la categoria en la instancia de la misma clase
    * @param nombre nombre de la cartegoria
    */ 
    public void newone(String nombre){        
        this.cate.setNombre(nombre);
    }
    
    /**
     * Metodo que recorre todas las categorias y las va almacenando en un objecto anonimo para poder entregarselas a la vista
     * @return ArrayList con objectos de categorias que contienen el id y el nombre
     */
    public static ArrayList<Object[]> index(){
        ArrayList<Object[]> categorias  = new ArrayList<>();

        for(Categoria temp : Categoria.all()) {
            Object[] objs = new Object[]{temp.getId(),temp.getNombre()};
            categorias.add(objs);
        }
        
        return categorias;
    } 
    
    /**
     * Por medio del parametro id, usando el metodo publico y estatico find de la clase Categoriase bsuca esa categoria en la BD
     * @param id id unico de la categroai dentro de la base de datos
     * @return boolean true si obtuvo correctamente el registro y false si no encontro el registro en la base de datos
     */
    public boolean get(Integer id){
        this.cate = Categoria.find(id);
        return this.cate.getId() != -1;
    }
    
    /**
     * Metodo para gatillar el guardar de la clase categoria, retornara el ID con el que se guardo en la BD
     * ya sea para actualizar o para la creación
     * @return Integer id de la entrada luego de guardar 
     */
    public Integer save(){
        Integer id;
        this.cate.save();
        id = this.cate.getId();
        return id;
    }
    
    /**
     * Metodo para consultar si la categoria dentro del controlador es una nueva o existente
     * @return boolean true si es nuevo registro o false si no lo es 
     */
    public boolean new_record(){
        return this.cate.getId() == -1;
    }
    
    public Object[] get_fields(){        
        Object[] objs = new Object[2]; 
        objs[0] = this.cate.getId();
        objs[1] = this.cate.getNombre();
        return objs;
    }
    
    /**
     * Metodo para destruir la categoria actual
     */
    public void eliminar(){
       this.cate.destroy();
       this.cate = new Categoria();
    }
    
}
