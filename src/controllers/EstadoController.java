/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import models.Estado;
/**
 *
 * @author jaimearroyo
 */
public class EstadoController {
    private Estado estado;
/**
 * Constructor que setea el estado como una nueva instancia de la clase Estado
 */
    public EstadoController() {
        this.estado = new Estado();
    }
    
   /**
    * Metodo que setea el nombre del estado en la instancia de la misma clase
    * @param estado nombre del estado
    */ 
    public void newone(String estado){        
        this.estado.setEstado(estado);
    }
    
    /**
     * Metodo que recorre todos los estados y los va almacenando en un objecto anonimo para poder entregarselas a la vista
     * @return ArrayList con objectos de estados que contienen el id y el estado
     */
    public static ArrayList<Object[]> index(){
        ArrayList<Object[]> estados  = new ArrayList<Object[]>();

        for(Estado temp : Estado.all()) {
            Object[] objs = new Object[]{temp.getId(),temp.getEstado()};
            estados.add(objs);
        }
        
        return estados;
    } 
    
    /**
     * Por medio del parametro id, usando el metodo publico y estatico find de la clase Estado se busca ese estado en la BD
     * @param id unico del estado dentro de la base de datos
     * @return boolean true si obtuvo correctamente el registro y false si no encontro el registro en la base de datos
     */
    public boolean get(Integer id){
        this.estado = Estado.find(id);
        return this.estado.getId() != -1;
    }
    
    /**
     * Metodo para gatillar el guardar de la clase Estado, retornara el ID con el que se guardo en la BD
     * ya sea para actualizar o para la creación
     * @return Integer id de la entrada luego de guardar 
     */
    public Integer save(){
        Integer id;
        this.estado.save();
        id = this.estado.getId();
        return id;
    }
    
    /**
     * Metodo para consultar si el estado dentro del controlador es una nueva o existente
     * @return boolean true si es nuevo registro o false si no lo es 
     */
    public boolean new_record(){
        return this.estado.getId() == -1;
    }
    
    /**
     * Obtiene los campos del objeto de la instancia Estado y los entrega en un objeto anonimo
     * @return Object anonimo con dos campos el id y el estado
     */
    public Object[] get_fields(){        
        Object[] objs = new Object[2]; 
        objs[0] = this.estado.getId();
        objs[1] = this.estado.getEstado();
        return objs;
    }
    
    /**
     * Metodo para destruir el estado actual
     */
    public void eliminar(){
       this.estado.destroy();
       this.estado = new Estado();
    }
    
}
