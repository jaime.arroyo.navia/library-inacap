/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import models.MetodoDePago;
/**
 *
 * @author jaimearroyo
 */
public class MetodoDePagoController {
    private MetodoDePago metodo_de_pago;
/**
 * Constructor que setea el metodo_de_pago como una nueva instancia de la clase MetodoDePago
 */
    public MetodoDePagoController() {
        this.metodo_de_pago = new MetodoDePago();
    }
    
   /**
    * Metodo que setea el nombre del metodo_de_pago en la instancia de la misma clase
    * @param metodo_de_pago nombre del metodo_de_pago
    */ 
    public void newone(String metodo_de_pago){        
        this.metodo_de_pago.setNombre(metodo_de_pago);
    }
    
    /**
     * Metodo que recorre todos los metodo_de_pagos y los va almacenando en un objecto anonimo para poder entregarselas a la vista
     * @return ArrayList con objectos de metodo_de_pagos que contienen el id y el metodo_de_pago
     */
    public static ArrayList<Object[]> index(){
        ArrayList<Object[]> metodo_de_pagos  = new ArrayList<Object[]>();

        for(MetodoDePago temp : MetodoDePago.all()) {
            Object[] objs = new Object[]{temp.getId(),temp.getNombre()};
            metodo_de_pagos.add(objs);
        }
        
        return metodo_de_pagos;
    } 
    
    /**
     * Por medio del parametro id, usando el metodo publico y estatico find de la clase MetodoDePago se busca ese metodo_de_pago en la BD
     * @param id unico del metodo_de_pago dentro de la base de datos
     * @return boolean true si obtuvo correctamente el registro y false si no encontro el registro en la base de datos
     */
    public boolean get(Integer id){
        this.metodo_de_pago = MetodoDePago.find(id);
        return this.metodo_de_pago.getId() != -1;
    }
    
    /**
     * Metodo para gatillar el guardar de la clase MetodoDePago, retornara el ID con el que se guardo en la BD
     * ya sea para actualizar o para la creación
     * @return Integer id de la entrada luego de guardar 
     */
    public Integer save(){
        Integer id;
        this.metodo_de_pago.save();
        id = this.metodo_de_pago.getId();
        return id;
    }
    
    /**
     * Metodo para consultar si el metodo_de_pago dentro del controlador es una nueva o existente
     * @return boolean true si es nuevo registro o false si no lo es 
     */
    public boolean new_record(){
        return this.metodo_de_pago.getId() == -1;
    }
    
    /**
     * Obtiene los campos del objeto de la instancia MetodoDePago y los entrega en un objeto anonimo
     * @return Object anonimo con dos campos el id y el metodo_de_pago
     */
    public Object[] get_fields(){        
        Object[] objs = new Object[2]; 
        objs[0] = this.metodo_de_pago.getId();
        objs[1] = this.metodo_de_pago.getNombre();
        return objs;
    }
    
    /**
     * Metodo para destruir el metodo_de_pago actual
     */
    public void eliminar(){
       this.metodo_de_pago.destroy();
       this.metodo_de_pago = new MetodoDePago();
    }
    
}
