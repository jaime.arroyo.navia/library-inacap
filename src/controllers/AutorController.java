/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import models.Autor;
/**
 *
 * @author jaimearroyo
 */
public class AutorController {
    private Autor autor;
/**
 * Constructor que setea el autor como una nueva instancia de la clase Autor
 */
    public AutorController() {
        this.autor = new Autor();
    }
    
/**
 * Setear los datos principales del autor
 * @param nombre Nombre del autor
 * @param apellido_pat Apellido paterno del autor
 * @param apellido_mat Apellido materno del autor
 */
    public void newone(String nombre, String apellido_pat, String apellido_mat){        
        this.autor.setNombre(nombre);
        this.autor.setApellido_paterno(apellido_pat);
        this.autor.setApellido_materno(apellido_mat);
    }
    
    /**
     * Metodo que recorre todos los autores y los va almacenando en un objecto anonimo para poder entregarselas a la vista
     * @return ArrayList con objectos de autores que contienen el id y el autor
     */
    public static ArrayList<Object[]> index(){
        ArrayList<Object[]> autores  = new ArrayList<>();

        for(Autor temp : Autor.all()) {
            Object[] objs = new Object[]{temp.getId(),temp.getNombre(),temp.getApellido_paterno(), temp.getApellido_materno()};
            autores.add(objs);
        }
        
        return autores;
    } 
    
    /**
     * Por medio del parametro id, usando el metodo publico y estatico find de la clase Autor se busca ese autor en la BD
     * @param id unico del autor dentro de la base de datos
     * @return boolean true si obtuvo correctamente el registro y false si no encontro el registro en la base de datos
     */
    public boolean get(Integer id){
        this.autor = Autor.find(id);
        return this.autor.getId() != -1;
    }
    
    /**
     * Metodo para gatillar el guardar de la clase Autor, retornara el ID con el que se guardo en la BD
     * ya sea para actualizar o para la creación
     * @return Integer id de la entrada luego de guardar 
     */
    public Integer save(){
        Integer id;
        this.autor.save();
        id = this.autor.getId();
        return id;
    }
    
    /**
     * Metodo para consultar si el autor dentro del controlador es una nueva o existente
     * @return boolean true si es nuevo registro o false si no lo es 
     */
    public boolean new_record(){
        return this.autor.getId() == -1;
    }
    
    /**
     * Obtiene los campos del objeto de la instancia Autor y los entrega en un objeto anonimo
     * @return Object anonimo con dos campos el id y el autor
     */
    public Object[] get_fields(){        
        Object[] objs = new Object[4]; 
        objs[0] = this.autor.getId();
        objs[1] = this.autor.getNombre();
        objs[2] = this.autor.getApellido_paterno();
        objs[3] = this.autor.getApellido_materno();
        return objs;
    }
    
    /**
     * Metodo para destruir el autor actual
     */
    public void eliminar(){
       this.autor.destroy();
       this.autor = new Autor();
    }
    
}
