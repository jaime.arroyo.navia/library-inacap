/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import models.Libro;
/**
 *
 * @author jaimearroyo
 */
public class LibroController {
    private Libro libro;
/**
 * Constructor que setea el libro como una nueva instancia de la clase Libro
 */
    public LibroController() {
        this.libro = new Libro();
    }
    
/**
 * Metodo para setear los datos principales del libro
 * @param isbn codiog isbn de libto
 * @param titulo del libro
 * @param num_paginas numero de paginas
 * @param precio_ref precio de referencia
 * @param anio año de publicacion
 * @param editoriales_id id de la editorial
 * @param ids_categorias ids en base de datos de las categorias
 * @param ids_autores id en base de datos de los autores
 */
    public void newone(String isbn, String titulo,Integer num_paginas, float precio_ref, Integer anio, Integer editoriales_id,ArrayList<Integer> ids_categorias, ArrayList<Integer> ids_autores){        
        this.libro.setIsbn(isbn);
        this.libro.setTitulo(titulo);
        this.libro.setNum_paginas(num_paginas);
        this.libro.setPrecio(precio_ref);
        this.libro.setAnio(anio);
        this.libro.setEditorial_id(editoriales_id);
        this.libro.setIds_autores(ids_autores);
        this.libro.setIds_categorias(ids_categorias);
    }
    
    /**
     * Metodo que recorre todos los libros y los va almacenando en un objecto anonimo para poder entregarselas a la vista
     * @return ArrayList con objectos de libros que contienen el id y el libro
     */
    public static ArrayList<Object[]> index(){
        ArrayList<Object[]> libros  = new ArrayList<Object[]>();

        for(Libro temp : Libro.all()) {
            
            EditorialController editcont = temp.getEditorial();
            Object[] objs_edit_cont = editcont.get_fields();
            Object[] objs = new Object[]{temp.getId(),temp.getIsbn(),temp.getTitulo(), temp.getNum_paginas(),temp.getPrecio(),temp.getAnio(),objs_edit_cont[1]};
            libros.add(objs);
        }
        
        return libros;
    } 
    
    /**
     * Por medio del parametro id, usando el metodo publico y estatico find de la clase Libro se busca ese libro en la BD
     * @param id unico del libro dentro de la base de datos
     * @return boolean true si obtuvo correctamente el registro y false si no encontro el registro en la base de datos
     */
    public boolean get(Integer id){
        this.libro = Libro.find(id);
        return this.libro.getId() != -1;
    }
    
    /**
     * Metodo para gatillar el guardar de la clase Libro, retornara el ID con el que se guardo en la BD
     * ya sea para actualizar o para la creación
     * @return Integer id de la entrada luego de guardar 
     */
    public Integer save(){
        Integer id;
        this.libro.save();
        id = this.libro.getId();
        return id;
    }
    
    /**
     * Metodo para consultar si el libro dentro del controlador es una nueva o existente
     * @return boolean true si es nuevo registro o false si no lo es 
     */
    public boolean new_record(){
        return this.libro.getId() == -1;
    }
    
    /**
     * Obtiene los campos del objeto de la instancia Libro y los entrega en un objeto anonimo
     * @return Object anonimo con los campos de la instancia de la clase Libros
     */
    public Object[] get_fields(){        
        Object[] objs = new Object[9]; 
        objs[0] = this.libro.getId();
        objs[1] = this.libro.getIsbn();
        objs[2] = this.libro.getTitulo();
        objs[3] = this.libro.getNum_paginas();
        objs[4] = this.libro.getPrecio();
        objs[5] = this.libro.getAnio();
        objs[6] = this.libro.getEditorial_id();
        objs[7] = this.libro.getIds_autores();
        objs[8] = this.libro.getIds_categorias();

        return objs;
    }
    
    /**
     * Metodo para destruir el libro actual
     */
    public void eliminar(){
       this.libro.destroy();
       this.libro = new Libro();
    }
    
}
