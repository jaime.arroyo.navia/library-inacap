/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;
import java.util.ArrayList;
import models.Idioma;
/**
 *
 * @author jaimearroyo
 */
public class IdiomaController {
    private Idioma idiom;
/**
 * Constructor que setea el idioma como una nueva instancia de la clase Idioma
 */
    public IdiomaController() {
        this.idiom = new Idioma();
    }
    
   /**
    * Metodo que setea el nombre del idioma en la instancia de la misma clase
    * @param idioma nombre del idioma
    */ 
    public void newone(String idioma){        
        this.idiom.setIdioma(idioma);
    }
    
    /**
     * Metodo que recorre todos los idiomas y los va almacenando en un objecto anonimo para poder entregarselas a la vista
     * @return ArrayList con objectos de idiomas que contienen el id y el idioma
     */
    public static ArrayList<Object[]> index(){
        ArrayList<Object[]> idiomas  = new ArrayList<Object[]>();

        for(Idioma temp : Idioma.all()) {
            Object[] objs = new Object[]{temp.getId(),temp.getIdioma()};
            idiomas.add(objs);
        }
        
        return idiomas;
    } 
    
    /**
     * Por medio del parametro id, usando el metodo publico y estatico find de la clase Idioma se busca ese idioma en la BD
     * @param id unico del idioma dentro de la base de datos
     * @return boolean true si obtuvo correctamente el registro y false si no encontro el registro en la base de datos
     */
    public boolean get(Integer id){
        this.idiom = Idioma.find(id);
        return this.idiom.getId() != -1;
    }
    
    /**
     * Metodo para gatillar el guardar de la clase Idioma, retornara el ID con el que se guardo en la BD
     * ya sea para actualizar o para la creación
     * @return Integer id de la entrada luego de guardar 
     */
    public Integer save(){
        Integer id;
        this.idiom.save();
        id = this.idiom.getId();
        return id;
    }
    
    /**
     * Metodo para consultar si el idioma dentro del controlador es una nueva o existente
     * @return boolean true si es nuevo registro o false si no lo es 
     */
    public boolean new_record(){
        return this.idiom.getId() == -1;
    }
    
    /**
     * Obtiene los campos del objeto de la instancia Idioma y los entrega en un objeto anonimo
     * @return Object anonimo con dos campos el id y el idioma
     */
    public Object[] get_fields(){        
        Object[] objs = new Object[2]; 
        objs[0] = this.idiom.getId();
        objs[1] = this.idiom.getIdioma();
        return objs;
    }
    
    /**
     * Metodo para destruir el idioma actual
     */
    public void eliminar(){
       this.idiom.destroy();
       this.idiom = new Idioma();
    }
    
}
