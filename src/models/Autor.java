/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;
import dao.DBConnection;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author jaimearroyo
 */
public class Autor {
    private int id;
    private String nombre;
    private String apellido_paterno;
    private String apellido_materno;
    
    static DBConnection mysqlConnect = new DBConnection();
    /**
     * constructor de la clase, setea el id en -1
     */
    public Autor() {
        this.id = -1;
    }
    /**
     * Obtiene el valor del id
     *
     * @return el valor de id
     */
    public int getId() {
        return id;
    }
    /**
     * Setea el valor de id
     *
     * @param id el nuevo valor de id
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * Obtiene el valor del nombre
     *
     * @return el valor de nombre
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Setea el valor de nombre
     *
     * @param nombre el nuevo valor de nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    /**
     * Obtiene el valor de apellido_paterno
     *
     * @return el valor de apellido_paterno
     */
    public String getApellido_paterno() {
        return apellido_paterno;
    }
    /**
     * Setea el valor de nombre
     *
     * @param apellido_paterno el nuevo valor de apellido_paterno
     */
    public void setApellido_paterno(String apellido_paterno) {
        this.apellido_paterno = apellido_paterno;
    }

     /**
     * getter del campo apellido_materno
     * @return string apellido_materno
     */
    public String getApellido_materno() {
        return apellido_materno;
    }

    /**
     * Setea el valor de apellido_materno
     *
     * @param apellido_materno el nuevo valor de apellido_materno
     */
    public void setApellido_materno(String apellido_materno) {
        this.apellido_materno = apellido_materno;
    }
   
    /**
     * Obtener un autor desde la base de datos con su ID
     * @param id del autor a buscar en la base de datos
     * @return una instancia del autor encontrado
     */    
    public static Autor find(int id){
        Autor nombre_var = new Autor();
        String sql = "SELECT * FROM `autores` where id = ?";
        
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            statement.setInt(1,id);
            
            ResultSet rs=statement.executeQuery();
            if(rs.first()){
                nombre_var.setId(rs.getInt("id"));
                nombre_var.setNombre(rs.getString("nombre"));
                nombre_var.setApellido_paterno(rs.getString("apellido_paterno"));
                nombre_var.setApellido_materno(rs.getString("apellido_materno"));
            }
            
            
        } catch (SQLException e) {
            System.err.println("Error al buscar en la base de datos el registro: "+id+" "+e);
        }catch(Exception e) {
            System.err.println("Error al buscar en la base de datos el registro: "+id+" "+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return nombre_var;
    }
    /**
     * Obtener todas los autores desde la base de datos
     * @return Un arreglo de instancias de la clase Autor
     */    
    public static ArrayList<Autor> all(){
        String sql = "SELECT * FROM `autores`";
        ArrayList<Autor> _listaAutores = new ArrayList<>();
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            ResultSet rs=statement.executeQuery();
            while (rs.next()) {
                Autor duenomas = new Autor();
                duenomas.setId(rs.getInt("id"));
                duenomas.setNombre(rs.getString("nombre"));
                duenomas.setApellido_paterno(rs.getString("apellido_paterno"));
                duenomas.setApellido_materno(rs.getString("apellido_materno"));
                _listaAutores.add(duenomas);
            }
        } catch (SQLException e) {
            System.err.println("Error al buscar en la base de datos todos los registros"+e);
        }catch(Exception e) {
            System.err.println("Error al buscar en la base de datos todos los registros"+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return _listaAutores;
    }
    /**
     * Ejecuta el metodo insert o update de la instancia en caso que corresponda, si es un registro nuevo se crea en el caso contrario
     * se actualiza en la base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */    
    public boolean save(){
        boolean result = false;
         System.out.println(this.getId());
         System.out.println(this.getId() == -1);
        if(this.getId() == -1){
            result = this.insert();
        }else{
            result = this.update();
        }
        
        return result;
    
    }
/**
 * Metodo que inserta un nuevo autor en la base de datos, 
 * luego de insertarlo se setea el id de la instancia actual con el id en base de datos
 * @return verdadero si se guardo correcramente o falso si no se realizo
 */     
    private boolean insert(){
        boolean result = false;
        String sql = "INSERT INTO autores(nombre,apellido_paterno,apellido_materno) Values(?,?,?)";
        try {
            int id = -1;
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,this.getNombre());
            statement.setString(2,this.getApellido_paterno());
            statement.setString(3,this.getApellido_materno());
            statement.execute();
            
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()){
               result = true;
               id=rs.getInt(1);
               System.err.println("Id retornado: "+id);
               this.setId(id);
            }else{
                result = false;
                System.err.println("No retorno nada");
            }

            
        } catch (SQLException e) {
            System.err.println("Error al ingresar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al ingresar en la base de datos "+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return result;
    }
/**
 * La instancia al estar con datos de la base de datos, ejecutara la actualización sus valores dentro de la base de datos
 * @return verdadero si se guardo correcramente o falso si no se realizo
 */    
    private boolean update(){
        boolean result = false;
        String sql = "UPDATE autores SET nombre = ?,apellido_paterno = ?, apellido_materno = ? WHERE id = ?";
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,this.getNombre());
            statement.setString(2,this.getApellido_paterno());
            statement.setString(3,this.getApellido_materno());
            statement.setInt(4,this.getId());
            result = statement.execute();
            
        } catch (SQLException e) {
            System.err.println("Error al actualizar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al actualizar en la base de datos "+e);
        } finally {
            mysqlConnect.disconnect();
        }        

        return result;
    }
 /**
  * Elimina de la base de datos el registro asociado a la instancia correspondiente por su id
  * @return verdadero si es que se elimino o falso si no se hizo correctamente
  */   
    public boolean destroy(){
        boolean result = false;
        String sql = "DELETE FROM autores WHERE id = ?";
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1,this.getId());

            result = statement.execute();
            if(result){
                
            }else{
                this.id = -1;
            }
            
        }catch (SQLException e) {
            System.err.println("Error al eliminar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al eliminar en la base de datos "+e);
        }finally {
            mysqlConnect.disconnect();
        }        

        return result;
    }
}
