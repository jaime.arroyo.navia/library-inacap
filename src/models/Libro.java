/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;
import controllers.AutorController;
import controllers.CategoriaController;
import controllers.EditorialController;
import dao.DBConnection;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author jaimearroyo
 */
public class Libro {
    private int id;
    private String titulo;
    private String isbn;
    private Integer num_paginas;
    private float precio;
    private int anio;
    private int editorial_id;
    private EditorialController editorial; 
    private ArrayList<Integer> ids_categorias;
    private ArrayList<Integer> ids_autores;
    private ArrayList<AutorController> autores_objects;
    private ArrayList<CategoriaController> categorias_objects;
    
    static DBConnection mysqlConnect = new DBConnection();
    /**
     * constructor de la clase, setea el id en -1
     */
    public Libro() {
        this.id = -1;
    }
    /**
     * Obtiene el valor de id
     *
     * @return el valor de id
     */
    public int getId() {
        return id;
    }
    /**
     * Setea el valor de id
     *
     * @param id el nuevo valor de id
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * Obtiene el valor de titulo
     *
     * @return el valor de titulo
     */
    public String getTitulo() {
        return titulo;
    }
    /**
     * Setea el valor de titulo
     *
     * @param titulo el nuevo valor de titulo
     */
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    /**
     * Obtiene el valor de isbn
     *
     * @return el valor de isbn
     */
    public String getIsbn() {
        return isbn;
    }
    /**
     * Setea el valor de isbn
     *
     * @param isbn el nuevo valor de isbn
     */
    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }
    /**
     * Obtiene el valor de num_paginas
     *
     * @return el valor de num_paginas
     */
    public Integer getNum_paginas() {
        return num_paginas;
    }
    /**
     * Setea el valor de num_paginas
     *
     * @param num_paginas el nuevo valor de num_paginas
     */
    public void setNum_paginas(Integer num_paginas) {
        this.num_paginas = num_paginas;
    }
    /**
     * Obtiene el valor de precio
     *
     * @return el valor de precio
     */
    public float getPrecio() {
        return precio;
    }
    /**
     * Setea el valor de precio
     *
     * @param precio el nuevo valor de precio
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }
    /**
     * Obtiene el valor de anio
     *
     * @return el valor de anio
     */
    public int getAnio() {
        return anio;
    }
    /**
     * Setea el valor de anio
     *
     * @param anio el nuevo valor de anio
     */
    public void setAnio(int anio) {
        this.anio = anio;
    }
    /**
     * Obtiene el valor de editorial_id
     *
     * @return el valor de editorial_id
     */
    public int getEditorial_id() {
        return editorial_id;
    }
    /**
     * Setea el valor de editorial_id
     *
     * @param editorial_id el nuevo valor de editorial_id
     */
    public void setEditorial_id(int editorial_id) {
        this.editorial_id = editorial_id;
    }
    /**
     * Obtiene el valor de editorial_id
     *
     * @return el valor de editorial_id
     */
    public EditorialController getEditorial() {
        return editorial;
    }
    /**
     * Setea el objecto de editorial
     *
     * @param editorial el nuevo objecto de editorial
     */
    public void setEditorial(EditorialController editorial) {
        this.editorial = editorial;
    }
    
    /**
     * Obtiene el valor de editorial_id
     *
     * @return el valor de editorial_id
     */
    public ArrayList<Integer> getIds_categorias() {
        return ids_categorias;
    }
    /**
     * Setea el arreglo de ids_categorias
     *
     * @param ids_categorias el nuevo arreglo de ids_categorias
     */
    public void setIds_categorias(ArrayList<Integer> ids_categorias) {
        this.ids_categorias = ids_categorias;
    }
    /**
     * Obtiene el valor de editorial_id
     *
     * @return el valor de editorial_id
     */
    public ArrayList<Integer> getIds_autores() {
        return ids_autores;
    }
    /**
     * Setea el arreglo de ids_autores
     *
     * @param ids_autores el nuevo arreglo de ids_autores
     */
    public void setIds_autores(ArrayList<Integer> ids_autores) {
        this.ids_autores = ids_autores;
    }
    /**
     * Obtiene el valor de editorial_id
     *
     * @return el valor de editorial_id
     */
    public ArrayList<AutorController> getAutores_objects() {
        return autores_objects;
    }
    /**
     * Setea los objectos de autores_objects
     *
     * @param autores_objects Los nuevos objetos de autores_objects
     */
    public void setAutores_objects(ArrayList<AutorController> autores_objects) {
        this.autores_objects = autores_objects;
    }
    /**
     * Obtiene el valor de editorial_id
     *
     * @return el valor de editorial_id
     */
    public ArrayList<CategoriaController> getCategorias_objects() {
        return categorias_objects;
    }
    /**
     * Setea los objectos de categorias_objects
     *
     * @param categorias_objects Los nuevos objetos de categorias_objects
     */
    public void setCategorias_objects(ArrayList<CategoriaController> categorias_objects) {
        this.categorias_objects = categorias_objects;
    }
    
    
    
    
    /**
     * Obtener un libro desde la base de datos con su ID, ademas se obtienen las categorias y autores del mismo
     * @param id del libro a buscar en la base de datos
     * @return una instancia del libro encontrado
     */   
    public static Libro find(int id){
        Libro nombre_var = new Libro();
        String sql = "SELECT * FROM `libros` where id = ?";
        
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            statement.setInt(1,id);
            
            ResultSet rs=statement.executeQuery();
            if(rs.first()){
                nombre_var.setId(rs.getInt("id"));
                nombre_var.setIsbn(rs.getString("isbn"));
                nombre_var.setTitulo(rs.getString("titulo"));
                nombre_var.setNum_paginas(rs.getInt("num_paginas"));
                nombre_var.setPrecio(rs.getFloat("precio_ref"));
                nombre_var.setAnio(rs.getInt("anio"));
                nombre_var.setEditorial_id(rs.getInt("editoriales_id"));
                
                EditorialController editori = new EditorialController();
                editori.get(rs.getInt("editoriales_id"));
                nombre_var.setEditorial(editori);
                
                sql = "SELECT * FROM `categorias_de_libros` where libros_id = ?";
                PreparedStatement statement2 = mysqlConnect.connect().prepareStatement(sql);
                statement2.setInt(1,id);
                ResultSet rs2=statement2.executeQuery();
                ArrayList<Integer> cat_local_ids  = new ArrayList<>();
                ArrayList<CategoriaController> cat_local_objects  = new ArrayList<>();
                while (rs2.next()) {
                    System.out.println("ID cat: "+rs2.getInt("categorias_id"));
                    cat_local_ids.add(rs2.getInt("categorias_id"));
                    CategoriaController cat_contro = new CategoriaController();
                    cat_contro.get(rs2.getInt("categorias_id"));
                    cat_local_objects.add(cat_contro);
                }
                nombre_var.setIds_categorias(cat_local_ids);
                nombre_var.setCategorias_objects(cat_local_objects);
                
                sql = "SELECT * FROM `autores_del_libro` where libros_id = ?";
                statement = mysqlConnect.connect().prepareStatement(sql);
                statement.setInt(1,id);
                rs=statement.executeQuery();
                
                ArrayList<Integer> aut_local_ids  = new ArrayList<>();
                ArrayList<AutorController> aut_local_objects  = new ArrayList<>();
                
                while (rs.next()) {
                    System.out.println("ID autor: "+rs.getInt("autores_id"));
                    aut_local_ids.add(rs.getInt("autores_id"));
                    AutorController aut_contro = new AutorController();
                    aut_contro.get(rs.getInt("autores_id"));
                    aut_local_objects.add(aut_contro);
                }
                nombre_var.setIds_autores(aut_local_ids);
                nombre_var.setAutores_objects(aut_local_objects);
                
            }
            
            
        } catch (SQLException e) {
            System.err.println("Error al buscar en la base de datos el registro: "+id+" "+e);
            e.printStackTrace();
        }catch(Exception e) {
            System.err.println("Error al buscar en la base de datos el registro: "+id+" "+e);
            e.printStackTrace();
        } finally {
            mysqlConnect.disconnect();
        }
        
        return nombre_var;
    }
    /**
     * Obtener todos los libros desde la base de datos
     * @return Un arreglo de instancias de la clase Libro
     */   
    public static ArrayList<Libro> all(){
        String sql = "SELECT * FROM `libros`";
        ArrayList<Libro> _listaAutores = new ArrayList<Libro>();
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            ResultSet rs=statement.executeQuery();
            while (rs.next()) {
                Libro duenomas = new Libro();
                duenomas.setId(rs.getInt("id"));
                duenomas.setIsbn(rs.getString("isbn"));
                duenomas.setTitulo(rs.getString("titulo"));
                duenomas.setNum_paginas(rs.getInt("num_paginas"));
                duenomas.setPrecio(rs.getFloat("precio_ref"));
                duenomas.setAnio(rs.getInt("anio"));
                duenomas.setAnio(rs.getInt("editoriales_id"));
                
                EditorialController editori = new EditorialController();
                editori.get(rs.getInt("editoriales_id"));
                duenomas.setEditorial(editori);
                
                _listaAutores.add(duenomas);
            }
        } catch (SQLException e) {
            System.err.println("Error al buscar en la base de datos todos los registros"+e);
        }catch(Exception e) {
            System.err.println("Error al buscar en la base de datos todos los registros"+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return _listaAutores;
    }
    /**
     * Ejecuta el metodo insert o update de la instancia en caso que corresponda, si es un registro nuevo se crea en el caso contrario
     * se actualiza en la base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */  
    public boolean save(){
        boolean result = false;
         System.out.println(this.getId());
         System.out.println(this.getId() == -1);
        if(this.getId() == -1){
            result = this.insert();
        }else{
            result = this.update();
        }
        
        return result;
    
    }
    /**
     * Metodo que inserta un nuevo autor en la base de datos, ademas de insertar sus categorias y autores
     * luego de insertarlo se setea el id de la instancia actual con el id en base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */   
    private boolean insert(){
        boolean result = false;
        String sql = "INSERT INTO libros(`isbn`, `titulo`, `num_paginas`, `precio_ref`, `anio`, `editoriales_id`) Values(?,?,?,?,?,?)";
        try {
            int id = -1;
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,this.getIsbn());
            statement.setString(2,this.getTitulo());
            statement.setInt(3, this.getNum_paginas());
            statement.setFloat(4, this.getPrecio());
            statement.setInt(5, this.getAnio());
            statement.setInt(6, this.getEditorial_id());
            statement.execute();
            
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()){
               result = true;
               id=rs.getInt(1);
               System.err.println("Id retornado: "+id);
               this.setId(id);
               
               if(!this.ids_autores.isEmpty()){
                   this.ids_autores.forEach((p) -> {
                       try {
                            System.err.println("ID autor: "+p);
                            String sql2 = "INSERT INTO autores_del_libro(`autores_id`, `libros_id`) Values(?,?)"; 
                            PreparedStatement statement2 = mysqlConnect.connect().prepareStatement(sql2, Statement.RETURN_GENERATED_KEYS);
                            statement2.setInt(1,p);
                            statement2.setInt(2,this.getId());
                            statement2.execute();
                        } catch (SQLException e) {
                            System.err.println("Error al ingresar en la base de datos "+e);
                        }catch(Exception e) {
                            System.err.println("Error al ingresar en la base de datos "+e);
                        } finally {
                            mysqlConnect.disconnect();
                        }
                   });
               }
               
               if(!this.ids_categorias.isEmpty()){
                   this.ids_categorias.forEach((p) -> {
                       try {
                            System.err.println("ID categoria: "+p);
                            String sql3 = "INSERT INTO categorias_de_libros(`categorias_id`, `libros_id`) Values(?,?)"; 
                            PreparedStatement statement3 = mysqlConnect.connect().prepareStatement(sql3, Statement.RETURN_GENERATED_KEYS);
                            statement3.setInt(1,p);
                            statement3.setInt(2,this.getId());
                            statement3.execute();
                        } catch (SQLException e) {
                            System.err.println("Error al ingresar en la base de datos "+e);
                        }catch(Exception e) {
                            System.err.println("Error al ingresar en la base de datos "+e);
                        } finally {
                            mysqlConnect.disconnect();
                        }
                   });
               }
               
            }else{
                result = false;
                System.err.println("No retorno nada");
            }

            
        } catch (SQLException e) {
            System.err.println("Error al ingresar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al ingresar en la base de datos "+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return result;
    }
    /**
     * La instancia al estar con datos de la base de datos, ejecutara la actualización sus valores dentro de la base de datos incluyendo autores y categorias
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */   
    private boolean update(){
        boolean result = false;
        String sql = "UPDATE libros SET `isbn` = ?, `titulo` = ?, `num_paginas` = ?, `precio_ref` = ?, `anio` = ?, `editoriales_id` = ? WHERE id = ?";
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,this.getIsbn());
            statement.setString(2,this.getTitulo());
            statement.setInt(3, this.getNum_paginas());
            statement.setFloat(4, this.getPrecio());
            statement.setInt(5, this.getAnio());
            statement.setInt(6, this.getEditorial_id());
            statement.setInt(7,this.getId());
            result = statement.execute();
            
            String sql0 = "DELETE FROM autores_del_libro WHERE libros_id = ?";
            PreparedStatement statement0 = mysqlConnect.connect().prepareStatement(sql0, Statement.RETURN_GENERATED_KEYS);
            statement0.setInt(1,this.getId());
            statement0.execute();
            if(!this.ids_autores.isEmpty()){
                   this.ids_autores.forEach((p) -> {
                       try {
                            System.err.println("ID autor: "+p);
                            String sql2 = "INSERT INTO autores_del_libro(`autores_id`, `libros_id`) Values(?,?)"; 
                            PreparedStatement statement2 = mysqlConnect.connect().prepareStatement(sql2, Statement.RETURN_GENERATED_KEYS);
                            statement2.setInt(1,p);
                            statement2.setInt(2,this.getId());
                            statement2.execute();
                        } catch (SQLException e) {
                            System.err.println("Error al ingresar en la base de datos "+e);
                        }catch(Exception e) {
                            System.err.println("Error al ingresar en la base de datos "+e);
                        } finally {
                            mysqlConnect.disconnect();
                        }
                   });
               }
               
            String sql25 = "DELETE FROM categorias_de_libros WHERE libros_id = ?";
            PreparedStatement statement25 = mysqlConnect.connect().prepareStatement(sql25, Statement.RETURN_GENERATED_KEYS);
            statement25.setInt(1,this.getId());
            statement25.execute();
               if(!this.ids_categorias.isEmpty()){
                   this.ids_categorias.forEach((p) -> {
                       try {
                            System.err.println("ID categoria: "+p);
                            String sql3 = "INSERT INTO categorias_de_libros(`categorias_id`, `libros_id`) Values(?,?)"; 
                            PreparedStatement statement3 = mysqlConnect.connect().prepareStatement(sql3, Statement.RETURN_GENERATED_KEYS);
                            statement3.setInt(1,p);
                            statement3.setInt(2,this.getId());
                            statement3.execute();
                        } catch (SQLException e) {
                            System.err.println("Error al ingresar en la base de datos "+e);
                        }catch(Exception e) {
                            System.err.println("Error al ingresar en la base de datos "+e);
                        } finally {
                            mysqlConnect.disconnect();
                        }
                   });
               }
            
            
        } catch (SQLException e) {
            System.err.println("Error al actualizar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al actualizar en la base de datos "+e);
        } finally {
            mysqlConnect.disconnect();
        }        

        return result;
    }
    /**
     * Elimina de la base de datos el registro asociado a la instancia correspondiente por su id
     * @return verdadero si es que se elimino o falso si no se hizo correctamente
     */   
    public boolean destroy(){
        boolean result = false;
        String sql = "DELETE FROM libros WHERE id = ?";
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1,this.getId());

            result = statement.execute();
            if(result){
                
            }else{
                this.id = -1;
            }
            
        }catch (SQLException e) {
            System.err.println("Error al eliminar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al eliminar en la base de datos "+e);
        }finally {
            mysqlConnect.disconnect();
        }        

        return result;
    }


}
