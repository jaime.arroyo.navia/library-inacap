/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;
import dao.DBConnection;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author jaimearroyo
 */
public class Categoria {
    private int id;
    private String nombre;

    static DBConnection mysqlConnect = new DBConnection();
/**
 * constructor de la clase, setea el id en -1
 */
    public Categoria() {
        this.id = -1;
    }
    /**
     * Obtiene el valor de id
     *
     * @return el valor de id
     */
    public int getId() {
        return id;
    }
    /**
     * Setea el valor de id
     *
     * @param id el nuevo valor de id
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * Obtiene el valor de nombre
     *
     * @return el valor de nombre
     */
    public String getNombre() {
        return nombre;
    }
    /**
     * Setea el valor de nombre
     *
     * @param nombre nuevo valor del nombre
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
   
 /**
  * Obtener una categoria desde la base de datos con su ID
  * @param id de la categoria a buscar en la base de datos
  * @return una instancia de la categoria encontrado
  */    
    public static Categoria find(int id){
        Categoria duenomas = new Categoria();
        String sql = "SELECT * FROM `categorias` where id = ?";
        
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            statement.setInt(1,id);
            
            ResultSet rs=statement.executeQuery();
            if(rs.first()){
                duenomas.setId(rs.getInt("id"));
                duenomas.setNombre(rs.getString("nombre"));
            }
            
            
        } catch (SQLException e) {
            System.err.println("Error al buscar en la base de datos el registro: "+id+" "+e);
        }catch(Exception e) {
            System.err.println("Error al buscar en la base de datos el registro: "+id+" "+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return duenomas;
    }
    /**
     * Obtener todas las categorias desde la base de datos
     * @return Un arreglo de instancias de la clase Autor
     */     
    public static ArrayList<Categoria> all(){
        String sql = "SELECT * FROM `categorias`";
        ArrayList<Categoria> _listaCategorias = new ArrayList<Categoria>();
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            ResultSet rs=statement.executeQuery();
            while (rs.next()) {
                Categoria duenomas = new Categoria();
                duenomas.setId(rs.getInt("id"));
                duenomas.setNombre(rs.getString("nombre"));
                _listaCategorias.add(duenomas);
            }
        } catch (SQLException e) {
            System.err.println("Error al buscar en la base de datos todos los registros"+e);
        }catch(Exception e) {
            System.err.println("Error al buscar en la base de datos todos los registros"+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return _listaCategorias;
    }
    /**
     * Ejecuta el metodo insert o update de la instancia en caso que corresponda, si es un registro nuevo se crea en el caso contrario
     * se actualiza en la base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */    
    public boolean save(){
        boolean result = false;
         System.out.println(this.getId());
         System.out.println(this.getId() == -1);
        if(this.getId() == -1){
            result = this.insert();
        }else{
            result = this.update();
        }
        
        return result;
    
    }
    /**
     * Metodo que inserta un nuevo autor en la base de datos, 
     * luego de insertarlo se setea el id de la instancia actual con el id en base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */       
    private boolean insert(){
        boolean result = false;
        String sql = "INSERT INTO categorias(nombre) Values(?)";
        try {
            int id = -1;
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,this.getNombre());
            
            statement.execute();
            
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()){
               result = true;
               id=rs.getInt(1);
               System.err.println("Id retornado: "+id);
               this.setId(id);
            }else{
                result = false;
                System.err.println("No retorno nada");
            }

            
        } catch (SQLException e) {
            System.err.println("Error al ingresar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al ingresar en la base de datos "+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return result;
    }
    /**
     * La instancia al estar con datos de la base de datos, ejecutara la actualización sus valores dentro de la base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */     
    private boolean update(){
        boolean result = false;
        String sql = "UPDATE categorias SET nombre = ? WHERE id = ?";
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,this.getNombre());
            statement.setInt(2,this.getId());
            result = statement.execute();
            
        } catch (SQLException e) {
            System.err.println("Error al actualizar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al actualizar en la base de datos "+e);
        } finally {
            mysqlConnect.disconnect();
        }        

        return result;
    }
    /**
     * Elimina de la base de datos el registro asociado a la instancia correspondiente por su id
     * @return verdadero si es que se elimino o falso si no se hizo correctamente
     */     
    public boolean destroy(){
        boolean result = false;
        String sql = "DELETE FROM categorias WHERE id = ?";
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1,this.getId());

            result = statement.execute();
            if(result){
                
            }else{
                this.id = -1;
            }
            
        }catch (SQLException e) {
            System.err.println("Error al eliminar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al eliminar en la base de datos "+e);
        }finally {
            mysqlConnect.disconnect();
        }        

        return result;
    }
}
