/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;
import dao.DBConnection;
import java.sql.*;
import java.util.ArrayList;

/**
 *
 * @author jaimearroyo
 */
public class Estado {
    private int id;
    private String estado;

    static DBConnection mysqlConnect = new DBConnection();
/**
 * constructor de la clase, setea el id en -1
 */
    public Estado() {
        this.id = -1;
    }
    /**
     * Obtiene el valor de id
     *
     * @return el valor de id
     */
    public int getId() {
        return id;
    }
    /**
     * Setea el valor de id
     *
     * @param id el nuevo valor de id
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     * Obtiene el valor de estado
     *
     * @return el valor de estado
     */
    public String getEstado() {
        return estado;
    }
    /**
     * Setea el valor de estado
     *
     * @param estado nuevo valor de estado
     */
    public void setEstado(String estado) {
        this.estado = estado;
    }
   
    /**
     * Obtener un estado desde la base de datos con su ID
     * @param id del estado a buscar en la base de datos
     * @return una instancia del estado encontrado
     */   
    public static Estado find(int id){
        Estado estado_var = new Estado();
        String sql = "SELECT * FROM `estados_libros` where id = ?";
        
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            statement.setInt(1,id);
            
            ResultSet rs=statement.executeQuery();
            if(rs.first()){
                estado_var.setId(rs.getInt("id"));
                estado_var.setEstado(rs.getString("estado"));
            }
            
            
        } catch (SQLException e) {
            System.err.println("Error al buscar en la base de datos el registro: "+id+" "+e);
        }catch(Exception e) {
            System.err.println("Error al buscar en la base de datos el registro: "+id+" "+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return estado_var;
    }
    
    /**
     * Obtener todos los estados desde la base de datos
     * @return Un arreglo de instancias de la clase Estado
     */   
    public static ArrayList<Estado> all(){
        String sql = "SELECT * FROM `estados_libros`";
        ArrayList<Estado> _listaIdiomas = new ArrayList<Estado>();
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql);
            ResultSet rs=statement.executeQuery();
            while (rs.next()) {
                Estado duenomas = new Estado();
                duenomas.setId(rs.getInt("id"));
                duenomas.setEstado(rs.getString("estado"));
                _listaIdiomas.add(duenomas);
            }
        } catch (SQLException e) {
            System.err.println("Error al buscar en la base de datos todos los registros"+e);
        }catch(Exception e) {
            System.err.println("Error al buscar en la base de datos todos los registros"+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return _listaIdiomas;
    }
    /**
     * Ejecuta el metodo insert o update de la instancia en caso que corresponda, si es un registro nuevo se crea en el caso contrario
     * se actualiza en la base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */  
    public boolean save(){
        boolean result = false;
         System.out.println(this.getId());
         System.out.println(this.getId() == -1);
        if(this.getId() == -1){
            result = this.insert();
        }else{
            result = this.update();
        }
        
        return result;
    
    }
    /**
     * Metodo que inserta un nuevo autor en la base de datos, 
     * luego de insertarlo se setea el id de la instancia actual con el id en base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */     
    private boolean insert(){
        boolean result = false;
        String sql = "INSERT INTO estados_libros(estado) Values(?)";
        try {
            int id = -1;
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,this.getEstado());
            
            statement.execute();
            
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()){
               result = true;
               id=rs.getInt(1);
               System.err.println("Id retornado: "+id);
               this.setId(id);
            }else{
                result = false;
                System.err.println("No retorno nada");
            }

            
        } catch (SQLException e) {
            System.err.println("Error al ingresar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al ingresar en la base de datos "+e);
        } finally {
            mysqlConnect.disconnect();
        }
        
        return result;
    }
    /**
     * La instancia al estar con datos de la base de datos, ejecutara la actualización sus valores dentro de la base de datos
     * @return verdadero si se guardo correcramente o falso si no se realizo
     */    
    private boolean update(){
        boolean result = false;
        String sql = "UPDATE estados_libros SET estado = ? WHERE id = ?";
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1,this.getEstado());
            statement.setInt(2,this.getId());
            result = statement.execute();
            
        } catch (SQLException e) {
            System.err.println("Error al actualizar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al actualizar en la base de datos "+e);
        } finally {
            mysqlConnect.disconnect();
        }        

        return result;
    }
    /**
     * Elimina de la base de datos el registro asociado a la instancia correspondiente por su id
     * @return verdadero si es que se elimino o falso si no se hizo correctamente
     */  
    public boolean destroy(){
        boolean result = false;
        String sql = "DELETE FROM estados_libros WHERE id = ?";
        try {
            PreparedStatement statement = mysqlConnect.connect().prepareStatement(sql, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1,this.getId());

            result = statement.execute();
            if(result){
                
            }else{
                this.id = -1;
            }
            
        }catch (SQLException e) {
            System.err.println("Error al eliminar en la base de datos "+e);
        }catch(Exception e) {
            System.err.println("Error al eliminar en la base de datos "+e);
        }finally {
            mysqlConnect.disconnect();
        }        

        return result;
    }
}
